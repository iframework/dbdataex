package com.dbex.core;

import com.dbex.util.MainConfig;

/**
 * 导出源数据库中的表
 * 扫描源数据库，读取各表，导出create,insert语句。
 * @author lxx
 *
 */
public class Export {
	
	
	
	/**
	 * 源表方言
	 */
	static Dialect srcDialect;
	
	/**
	 * 目的表方言
	 */
	static Dialect desDialect;  
	
	/**
	 * 导出 SQL 语句
	 */
	@SuppressWarnings({  "rawtypes" })
	public void export(){
		try {
			long s0 = System.currentTimeMillis();
			Class c = Class.forName(MainConfig.getValue("src.dialect"));
			Class d = Class.forName(MainConfig.getValue("des.dialect"));
			srcDialect = (Dialect)c.newInstance();
			desDialect = (Dialect)d.newInstance();
			OutputSQL outputSQL = new OutputSQL(srcDialect.getAllTables(), srcDialect, desDialect, MainConfig.getValue("des.schema"));
			outputSQL.writeSQL();
			long s1 = System.currentTimeMillis();
			System.out.println("导出成功！耗时："+(s1-s0));
		} catch (Exception e) {
			System.out.println("导出失败！");
			e.printStackTrace();
		} 
		
	}
	
	

	public static void main(String[] args) {
		new Export().export();
	}

}
