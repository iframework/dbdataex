package com.dbex.util;

public class StringUtils {
	
	public static String capital(String str){
		if(str==null) return "";
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i); 
			if(c>='a' && c<='z'){
				sb.append((char)(c-32));
			}else{
				sb.append(c);
			}
		}
		return sb.toString();
	}
	
	public static void main(String[] args) {
		System.out.println(capital(""));
	}

}
