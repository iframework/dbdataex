package com.dbex.util;

/**
 * 系统常量
 * @author lxx
 *
 */
public interface Constant {
	
	/**
	 * 默认的 oracle 驱动
	 */
	String DEFAULT_ORACLE_DRIVER_CLASS_NAME = "oracle.jdbc.driver.OracleDriver";
	
	/**
	 * 默认的 mysql 驱动 
	 */
	String DEFAULT_MYSQL_DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";
	
	/**
	 * oracle 查找当前登录用户所有的表 
	 */
	String ORACLE_FIND_USER_TABLES = "select table_name from user_tables";
	
	/**
	 * oracle 查找数据库中所有的表 
	 */
	String ORACLE_FIND_ALL_TABLES = "select table_name from all_tables at where 1 = 1";
	
	/**
	 * 查找某个表的所有列名，类型，以及注释
	 */
	String ORACLE_FIND_COLS_OF_TABLE = "select acc.column_name,atc.data_type,atc.data_length,atc.data_precision,atc.data_scale, acc.comments from all_tab_cols atc left outer join all_col_comments acc on (atc.owner=acc.owner and atc.table_name=acc.table_name and atc.column_name = acc.column_name )  where 1 = 1";
	
	String DATE_FORMAT = "yyyy-";
	
}
